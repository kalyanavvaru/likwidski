package com.likwidski.domain.model.user;

import com.likwidski.domain.shared.Entity;

/**
 * This is a User entity object as it has identity
 * 
 * @author kalyan
 * 
 */
public class User implements Entity<User> {
	private long userid;
	private String firstname;
	private String lastname;
	private String username;

	protected User() {
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public long getUserid() {
		return this.userid;
	}

	public void setUserid(final long userid) {
		this.userid = userid;
	}

	public void setFirstname(final String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(final String lastname) {
		this.lastname = lastname;
	}

	@Override
	public boolean sameIdentityAs(final User other) {
		return (other != null) && (this.userid == other.userid);
	}
}
