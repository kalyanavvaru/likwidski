package com.likwidski.domain.model.user;

import java.util.List;

/**
 * This is a repository interface which is agnostic of infrastructure
 * implementation(memory, database with jdbc, database with an ORM like
 * hibernate)
 * 
 * @author kalyan
 * 
 */
public interface UserRepository {
	public List<User> findAll();

	public User findById(final long userid);

	public User findByUsername(final String username);

	public void save(User user);
}
