package com.likwidski.domain.model.user;

/**
 * This is a value object as it has no identity of its own and can be immutable and reused.
 * 
 * @author kalyan
 * 
 */
public class Address {
    private String streetNumber;
    private String streetName;
    private String suburb;
    private String state;
    private String country;
    private String postcode;
    
    public String getStreetNumber() {
        return this.streetNumber;
    }
    
    public void setStreetNumber(final String streetNumber) {
        this.streetNumber = streetNumber;
    }
    
    public String getStreetName() {
        return this.streetName;
    }
    
    public void setStreetName(final String streetName) {
        this.streetName = streetName;
    }
    
    public String getSuburb() {
        return this.suburb;
    }
    
    public void setSuburb(final String suburb) {
        this.suburb = suburb;
    }
    
    public String getState() {
        return this.state;
    }
    
    public void setState(final String state) {
        this.state = state;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(final String country) {
        this.country = country;
    }
    
    public String getPostcode() {
        return this.postcode;
    }
    
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }
    
}
