package com.likwidski.infrastructure.persistence.db.hibernate;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.likwidski.domain.model.user.User;
import com.likwidski.domain.model.user.UserRepository;

@Repository
public class UserRepositoryHibernate extends HibernateRepository implements
		UserRepository {

	@Override
	public List<User> findAll() {
		return this.getSession().createQuery("from User").list();
	}

	@Override
	public User findByUsername(final String username) {
		return (User) this.getSession()
				.createQuery("from User where username = :username")
				.setParameter("username", username).uniqueResult();
	}

	@Override
	public User findById(final long userid) {
		return (User) this.getSession()
				.createQuery("from User where userid = :userid")
				.setParameter("userid", userid).uniqueResult();
	}

	@Override
	public void save(final User user) {
		this.getSession().saveOrUpdate(user);
	}
}
