package com.likwidski.infrastructure.persistence.db.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

/**
 * Functionality common to all Hibernate repositories.
 */
@Transactional
public abstract class HibernateRepository {
    
    private SessionFactory sessionFactory;
    
    @Required
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    protected Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }
    
}
