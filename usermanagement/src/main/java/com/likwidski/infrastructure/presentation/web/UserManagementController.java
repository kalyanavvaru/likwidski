package com.likwidski.infrastructure.presentation.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.likwidski.domain.model.user.User;
import com.likwidski.domain.model.user.UserRepository;

@Controller
@RequestMapping("/user")
public class UserManagementController {

	private UserRepository userRepository;

	@RequestMapping(value = "/username={username:.+}", method = RequestMethod.GET)
	public String getUser(@PathVariable final String username,
			final ModelMap model) {
		User user = this.userRepository.findByUsername(username);
		model.addAttribute("user", user);
		return "userCRUD";
	}

	@RequestMapping(value = "/{userid:.+}", method = RequestMethod.GET)
	public @ResponseBody
	User getUser(@PathVariable final long userid, final ModelMap model) {
		User user = this.userRepository.findById(userid);
		return user;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody
	List<User> getAllusers(final ModelMap model) {
		List<User> users = this.userRepository.findAll();
		return users;
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody
	User saveUser(@RequestBody final User user) {
		this.userRepository.save(user);
		return user;
	}

	public UserRepository getUserRepository() {
		return this.userRepository;
	}

	@Autowired
	public void setUserRepository(final UserRepository userRepository) {
		this.userRepository = userRepository;
	}
}
